# Optionals

Eine weitere wichtige Neuerung von Java 8 sind die Optionals. Diese können dazu
genutzt werden um not-null-Objekte zu behandeln.

Im Endeffekt sind sie nichts anderes als Container Objekte die das entsprechende
Element enthalten oder eben leer sind falls das Objekt null ist. Daher sind sie 
vor allem bei Datenbankabfragen oder bei Transferobjekten sinnvoll, eben genau dann,
wenn wir nicht sicherstellen können, ob Objekte null sind oder nicht. Jedoch können
sie auch in der Businesslogik durchaus sinnvoll sein.

## Grundsätzliche Nutzung

Warum das untere Beispiel mit normalen null-Prüfungen nicht schön ist, muss wohl
nicht erklärt werden. Zum einen ist die Übersichtlichkeit nicht sonderlich gut,
zum anderen müsste diese Prüfung an vielen Stellen gemacht werden. Wenn ein Entwickler
später Code ergänzt oder anpasst weiß er vielleicht nicht was alles null sein kann,
wodurch Fehler auftreten können. Besser ist es daher auf Optionals zu setzen.
Damit können zwar immernoch die gleichen Fehler passieren, allerdings ist dem 
Entwickler klar, wenn er ein Optional als Wert der Getter bekommt, dass die Ergebnisse
potentiell nullable sind.

```java
    Car car = getCar();
    
    if(car != null){
        Manufacturer manufacturer = car.getManufacturer();
        if(manufacturer != null){
            String name = manufacturer.getName();
            if(name != null) {
                return name
            }
            throw new Exception("Not found")
        }
        throw new Exception("Not found")
    }
    throw new Exception("Not found")
}
```

Das obige Beispiel lässt sich relativ einfach mit Optionals nachbilden:

```java
    Optional<Car> car = Optional.ofNullable(getCar());

    return car.flatMap(c -> Optional.ofNullable(c.getManufacturer()))
              .flatMap(m -> Optional.ofNullable(m.getName()))
              .orElseThrow(() -> new Exception("Not found"));
}
```

Die Funktionalität mit Optionals ist deutlich einfacher zu lesen und weniger
fehleranfällig, lässt sich aber noch weiter vereinfachen, wenn die Felder die 
theoretisch null sein dürfen in unserer Car Klasse als Optional definiert sind,
würden uns die Getter direkt wieder Optionals liefern, die entweder den entsprechenden
Wert enthalten oder eben leer sind. Auch unsere Datenbankabfrage (getCar()) sollte
ein Optional liefern. Damit würde foldender Code entstehen

```java
    Optional<Car> car = getCar();

    return car.flatMap(c -> c.getManufacturer())
              .flatMap(m -> m.getName())
              .orElseThrow(() -> new Exception("Not found"));
}
```
## Optional Methoden

### Erzeugung eines Optionals

Um ein Optional zu erzeugen gibt es 3 statische Methoden:

- Optional.empty() --> Erzeugt ein leeres Optional
- Optional.of(T value) --> Erzeugt ein Optional von value, value darf nicht null sein
- Optional.ofNullable(T value) --> --> Erzeugt ein Optional von value, value darf null sein

### Optiona.get()

Diese Methode liefert den Wert eines Optionals. Ist das Optional leer, wird eine **NoSuchElementException** geworfen.

```java
String result = Optional.empty().get() // NoSuchElementException
String result2 = Optional.of("Hello").get() // Hello

```
### Optional::filter

**Optional<T> filter(Predicate<? super <T>> predicate)**  

Diese Methode prüft den Wert des Optionals auf das gegebene Predicate, wird dieses 
erfüllt bekommt man ein Optional mit dem Wert zurück ansonsten ein leeres Optional.

### Optional::flatMap & Optional::map

**<U>Optional<U> map(Function<? super T,? extends U> mapper)**

Die map-Methode führt die Mapping-Funktion auf das Optional aus. Dabei ist wichtig,
dass der Rückgabewert kein Optional sein sollte, da wir sonst eine Verschachtelung
von Optionals erhalten. 

```java
test.map(t -> t.doSomething()) // ggfs Verschachtelung Optional<Optional<SomeThing>> 
```

Diese Verschachtelung kann man verhindern, indem man folgende Methode nutzt.

**<U> Optional<U> flatMap(Function<? super T,Optional<U>> mapper)**  

Optional.flatMap bekommt eine mapping-Funktion als Parameter, diese wird auf das Optional
angewendet und gibt dann den Wert dieser Funktion zurück. Diese Methode kann man auch im
ersten Beispiel oben sehen. Wenn kein Ergebnis gebildet werden kann, wird ein leeres Optional 
zurückgegeben werden.

```java
test.flatMap(t -> t.doSomething()).get() // liefert den Rückgabewert von doSomething

```



### Optional::isPresent

Die isPresent Methode ist überladen, sie kann entweder ohne Parameter aufgerufen werden,
dann liefert sie einen boolean Wert. Dieses Verhalten ist dann analog zu einem Null-Check.
Alternativ kann die isPresent-Methode auch mit einer Consumer-Funktion aufgerufen werden.
Wenn dann ein Wert im Optional vorhanden ist, wird er an den Consumer übergeben, ansonsten
passiert nichts.

```java
boolean exists = someOptional.isPresent();

someOptional.ifPresent(s -> doSomething());

```

### Errorhandling und Ausnahmebehandlung bei Optionals

- orElse
- orElseGet
- orElseThrow

Ist ein Optional leer, so wollen wir ja aber trotzdem mit der Programmlogik weitermachen,
der einfachste Fall ist, wir werfen eine aufbereitete Exception. Hierfür können wir die 
Methode **orElseThrow** verwenden. Diese bekommt einen Supplier, also ein Lambda-Ausdruck 
welcher eine Exception erzeugt.

```
Car car = carOpt.orElseThrow(() -> new SpecialException("Kein Auto gefunden"));

```

Möchten wir hingegen einen Defaultwert oder eine spezielles "NullObjekt" zurückgeben, so
kan man die Methoden **orElse** und **orElseGet** verwenden. Die erste Methode liefert 
einfach einen Wert zurück, die zweite Methode bekommt wieder einen Supplier über den 
ein Defaultwert besorgt werden kann.

```java
Car car = carOpt.orElse(DEFAULT_AUTO);

Car car2 = carOpt.orElseGet(() -> erstelleDefaultAuto());

```







