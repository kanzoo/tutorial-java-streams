package optionals;

import static java.util.Optional.ofNullable;

import java.util.Optional;

import data.DataCreator;
import models.City;
import models.Person;

public class OptionalTutorialSolution {

    private static final City DEFAULT_CITY = DataCreator.DEFAULT_CITY;

    private DataCreator dataCreator = new DataCreator();

    public String getHomeTownNameOfPerson(Person person) {
        Optional<Person> personOpt = Optional.of(person);

        return personOpt.flatMap(p -> ofNullable(p.getAdress()))
                .flatMap(a -> ofNullable(a.getCity()))
                .flatMap(c -> ofNullable(c.getName()))
                .orElse(DEFAULT_CITY.getName());
    }

    public String getHomeTownNameOfPersonWithDefaultMethod(Person person) {
        Optional<Person> personOpt = Optional.of(person);

        return personOpt.flatMap(p -> ofNullable(p.getAdress()))
                .flatMap(a -> ofNullable(a.getCity()))
                .flatMap(c -> ofNullable(c.getName()))
                .orElseGet(() -> dataCreator.getDefaultCity().getName());
    }

    public String getStreetNameOfPerson(Person person) throws Exception {
        Optional<Person> personOpt = Optional.of(person);

        return personOpt.flatMap(p -> ofNullable(p.getAdress()))
                .flatMap(a -> ofNullable(a.getStreetName()))
                .orElseThrow(() -> new Exception("Keine Adresse gesetzt"));
    }

}
