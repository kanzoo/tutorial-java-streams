package streams;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import data.DataCreator;
import models.Address;
import models.Person;

public class StreamtutorialTest {

    private Streamtutorial underTest;

    @BeforeEach
    void setUp() throws Exception {
        underTest = new Streamtutorial();
    }

    // Exercise 1

    @Test
    void testGetPersonsByLastName() {
        List<Person> result = underTest.getPersonsByLastName("M�ller");

        assertThat(result).hasSize(3);
        assertThat(result).containsExactly(DataCreator.PERSON_2, DataCreator.PERSON_5, DataCreator.PERSON_7);
    }

    // Exercise 2

    @Test
    void testGetPersonByFullName() {
        List<Person> result = underTest.getPersonsByFullName("Thomas", "M�ller");

        assertThat(result).hasSize(1);
        assertThat(result).containsExactly(DataCreator.PERSON_7);
    }

    // Exercise 3

    @Test
    void testGetPersonByFullNameWithPredicates() {
        List<Person> result = underTest.getPersonsByFullNameWithPredicates("Thomas", "M�ller");

        assertThat(result).hasSize(1);
        assertThat(result).containsExactly(DataCreator.PERSON_7);
    }

    // Exercise 4

    @Test
    void testGetAddressesOfPersons() {
        List<Address> result = underTest.getAddressesOfPersons();

        assertThat(result).hasSize(14);
        assertThat(result).containsExactly(DataCreator.ADRESSE_1, DataCreator.ADRESSE_1, DataCreator.ADRESSE_2,
                DataCreator.ADRESSE_2, DataCreator.ADRESSE_2, DataCreator.ADRESSE_3, DataCreator.ADRESSE_4, DataCreator.ADRESSE_5,
                DataCreator.ADRESSE_6, DataCreator.ADRESSE_7, DataCreator.ADRESSE_8, DataCreator.ADRESSE_9,
                DataCreator.ADRESSE_10, DataCreator.ADRESSE_10);
    }

    // Exercise 5

    @Test
    void testGetDistinctAddressesOfPersons() {
        List<Address> result = underTest.getDistinctAddressesOfPersons();

        assertThat(result).hasSize(10);
        assertThat(result).containsExactly(DataCreator.ADRESSE_1, DataCreator.ADRESSE_2, DataCreator.ADRESSE_3,
                DataCreator.ADRESSE_4, DataCreator.ADRESSE_5, DataCreator.ADRESSE_6, DataCreator.ADRESSE_7, DataCreator.ADRESSE_8,
                DataCreator.ADRESSE_9, DataCreator.ADRESSE_10);
    }

    // Exercise 6

    @Test
    void testGetFirstPersonFromCity() throws Exception {
        Person result = underTest.getFirstPersonFromCity(DataCreator.KARLSRUHE);

        assertThat(result).isEqualTo(DataCreator.PERSON_1);
    }

    @Test
    void testGetFirstPersonFromCityThrowsExcetption() throws Exception {
        assertThrows(Exception.class, () -> {
            underTest.getFirstPersonFromCity(DataCreator.STUTTGART);
        });
    }

    // Exercise 7

    @Test
    void testCreatePersonList() {
        List<Person> result = underTest
                .createPersonList(Arrays.asList(DataCreator.getKarlsruher(), DataCreator.getKirchheimer()));

        assertThat(result).containsExactly(DataCreator.PERSON_1, DataCreator.PERSON_2, DataCreator.PERSON_3, DataCreator.PERSON_4,
                DataCreator.PERSON_5, DataCreator.PERSON_6, DataCreator.PERSON_7, DataCreator.PERSON_8, DataCreator.PERSON_11,
                DataCreator.PERSON_12, DataCreator.PERSON_13, DataCreator.PERSON_14);
    }

    // Exercise 8

    @Test
    void testCountPersonsOfCity() {
        long result = underTest.countPersonsOfCity(DataCreator.KARLSRUHE);

        assertThat(result).isEqualTo(8);
    }

    // Exercise 9

    @Test
    void testSortPersonsOfCityByNachname() {
        List<Person> result = underTest.sortPersonsOfCityByNachname(DataCreator.KARLSRUHE);

        assertThat(result).containsExactly(DataCreator.PERSON_1, DataCreator.PERSON_4, DataCreator.PERSON_2, DataCreator.PERSON_5,
                DataCreator.PERSON_7, DataCreator.PERSON_8, DataCreator.PERSON_6, DataCreator.PERSON_3);
    }

    // Exercise 10

    @Test
    void testCheckPersonLivesInCityTrue() {
        boolean result = underTest.checkPersonLivesInCity(DataCreator.PERSON_1.getFirstName(), DataCreator.PERSON_1.getLastName(),
                DataCreator.KARLSRUHE);

        assertThat(result).isTrue();
    }

    @Test
    void testCheckPersonLivesInCityFalse() {
        boolean result = underTest.checkPersonLivesInCity(DataCreator.PERSON_9.getFirstName(), DataCreator.PERSON_9.getLastName(),
                DataCreator.KARLSRUHE);

        assertThat(result).isFalse();
    }

    // Exercise 11

    @Test
    void testGroupPersonsByCity() {
        Map<String, List<Person>> result = underTest.groupPersonsByCity();

        assertThat(result.entrySet()).hasSize(3);
        assertThat(result.get(DataCreator.KARLSRUHE.getName())).hasSize(8);
        assertThat(result.get(DataCreator.KARLSRUHE.getName())).containsExactly(DataCreator.PERSON_1, DataCreator.PERSON_2,
                DataCreator.PERSON_3, DataCreator.PERSON_4, DataCreator.PERSON_5, DataCreator.PERSON_6, DataCreator.PERSON_7,
                DataCreator.PERSON_8);
        assertThat(result.get(DataCreator.STUTENSEE.getName())).hasSize(2);
        assertThat(result.get(DataCreator.STUTENSEE.getName())).containsExactly(DataCreator.PERSON_9, DataCreator.PERSON_10);
        assertThat(result.get(DataCreator.KIRCHHEIM.getName())).hasSize(4);
        assertThat(result.get(DataCreator.KIRCHHEIM.getName())).containsExactly(DataCreator.PERSON_11, DataCreator.PERSON_12,
                DataCreator.PERSON_13, DataCreator.PERSON_14);
    }

    // Exercise 12

    @Test
    void testCreateSortedFirstNameList() {
        String result = underTest.createSortedFirstNameList();

        String commaSeperatedNameList = "Bastian, Bianca, Carl, Gerda, Jerome, Karl, Lukas, Manuel, Marco, Pascal, Robert, Stefan, Stefanie, Thomas";

        assertThat(result).isEqualTo(commaSeperatedNameList);
    }

}
