package models;

public class Person {

    private String firstName;
    private String lastName;
    private Address address;

    public Person(String surName, String lastName, Address address) {
        this.firstName = surName;
        this.lastName = lastName;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Address getAdress() {
        return address;
    }

}
