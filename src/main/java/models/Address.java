package models;

public class Address {

    private String streetName;
    private int number;
    private City city;

    public Address(String streetName, int number, City city) {
        this.streetName = streetName;
        this.number = number;
        this.city = city;
    }

    public String getStreetName() {
        return streetName;
    }

    public int getNumber() {
        return number;
    }

    public City getCity() {
        return city;
    }

}
