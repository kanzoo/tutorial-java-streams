# Java 8 Streams

## Inhalt des Tutorials

[Filtern von Elementen](#finden-eines-elementes-in-einer-liste)  
[Ausführen von Methoden auf Streamelementen](#ausführen-von-methoden-für-alle-elemente-einer-liste)  
[Sammeln der Ergebnisse von Streamoperationen](#collectors-für-streams)  
[Sortieren von Streams](#sortieren-von-streams)  

Mit dem Update auf Java 8 wurden unter anderem die Streams eingeführt. In Java ist
ein Stream im Endeffekt nichts anderes als eine Abfolge von Objekten. Damit wird
es ermöglicht, die an vielen Stellen benötigten for-/forEach Schleifen abzulösen und
durch einfache leicht lesbare Ausdrücke zu ersetzen. Für viele Use-Cases, für die
man vor Java 8 eine for-Schleife benötigt hat, bringt die Stream API einfache 
vorgefertigte Methoden mit. Eine weitere Neuerung, die mit Java 8 eingeführt wurde
sind die Lambda Ausdrücke. Diese machen die Stream API zu einem noch mächtigeren
Werkzeug.

Im folgenden Tutorial sollen eingie der Kernfeatures der Stream API vorgestellt werden.

## Finden eines Elementes in einer Liste 

Um ein Element in einer Liste zu finden, konvertieren wir die Liste zuerst zu 
einem Stream. Hier für reicht der Aufruf einer Methode: 

**list.stream()**  

Nun haben wir einen Stream, auf dem wir die API-Methoden ausführen können. Eine der 
wichtigsten Methoden ist die filter-Methode:  

**Stream<T> filter(Predicate<? super T> predicate)**  

Die filter-Methode bekommt ein Predicate als Parameter und liefert einen neuen Stream
mit allen Elementen die das Predicate "satisfyen", also es erfüllen. Ein Predicate ist
nichts anderes, als eine Aussage über ein Objekt. Trifft diese zu, gilt das Predicate 
als erfüllt. Ein Predicate ist ein sogenanntes funktionales Interface.

Da wir im unteren Beispiel im Predicate auf die Id prüfen, sollten wir einen Stream
mit genau einem Element erhalten. Daher können wir mit **findFirst()** dieses Element
zurückgeben, oder für den Fall, dass kein Element mit der gesuchten id existiert,
null zurückgeben.

Eine ähnliche Methode ist **findAny()**, diese gibt alle Elemente des Streams zurück.
Im Beispiel unten würden wir also eine Liste mit genau einem Element erhalten.

```java

// Mit Standard for-Schleife

public Car getCarById(List<Car> cars, long id) {
    for (int i = 0; i < cars.size(); i++) {
        if (cars.get(i).getId() == id) {
            return cars.get(i);
        }
    }
    return null;
}

// Mit Stream API

public Car getCarById(List<Car> cars, long id) {
    return cars.stream().filter(c -> c.getId() == id).findFirst().orElse(null);
}
```

Wenn wir mehrere Filterbedingungen haben, können wir die Predicates auch aus der 
Stream Chain herausziehen, dies erhöht die Lesbarkeit des Codes und bietet noch 
weitere Funktionalität. Haben wir eine die Predicates außerhalb als Variablen definiert
können wir diese mit Methoden verknüpfen die logische Operatoren abbilden. Wir können 
auch einen Predicate per Methode negieren. Hierfür gibt es eine **negate()** Methode.

```java

Predicate<Car> colorPredicate = (Car c) -> c.getColor().equals("black");
Predicate<Car> hpPredicate = (Car c) -> c.getHp() > 200;
Predicate<Car> manufacturerPredicate = (Car c) -> c.getManufacturer().equals("Ferrari");

// Wir wollen alle schwarzen Autos die mehr als 200 PS haben und zusätzlich alle Ferraris

List<Car> result = cars.stream()
    .filter(manufacturerPredicate
        .or(colorPredicate
        .and(hpPredicate)))
    .collect(Collectors.toList());

```


## Ausführen von Methoden für alle Elemente einer Liste

Wie die Überschrift schon sagt, wollen wir auf alle Elemente einer Liste eine Methode
ausführen. Hierfür stellt uns die API eine spezielle Methode bereit. 

**list.stream().map(Foo::doSomething)**

Der map-Methode wird also eine Funktion übergeben. Diese Funktion kann auch ein 
Lambda-Ausdruck sein.

Das untere Beispiel zeigt die Verwendung der map-Methode. Im gezeigten Beispiel
malen wir die Autos per Setter an, würden wir nicht die gegebene Liste verändern
sondern ein neues Auto in der maleAutoRotAn Methode erzeugen, müsste man in der 
Variante mit for-Schleife eine Anpassung vornehmen, die Stream API würde das ohne
Anpassung schaffen. In diesem Beispiel macht es zwar keinen Sinn, ein neues Auto 
zu erzeugen, da wir ja das bestehende Auto umlackieren wollen, allerdings sollte 
man mit dem "unsichtbaren" manipulieren von Elementen extrem vorsichtig sein.

```java

// Mit for-Schleife und Verändern der Referenz

public List<Car> paintCarsRed(List<Car> cars) {
    for (int i = 0; i < cars.size(); i++) {
        paintCarRed(cars.get(i));
    }
    return cars;
}

// Mit for-Schleife und Erzeugung einer neuen Referenz

public List<Car> paintCarsRed(List<Car> cars) {
    List<Car> redCars = new ArrayList();
    for (int i = 0; i < cars.size(); i++) {
        Car redCar = paintCarRed(cars.get(i));
        redCars.add(redCar);
    }
    return redCars;
}

// Mit Stream API, beide Fälle

public List<Car> paintCarsRed(List<Car> cars) {
    return cars.stream().map(this::paintCarRed).collect(Collectors.toList());
}

```

## Collectors für Streams

Im vorigen Beispiel haben wir bereits eine weitere neue Methode gesehen, die collect-
Methode. Diese wird dazu genutzt die Elemente die im Stream enthalten sind wieder 
zu "sammeln". Hierfür stellt sie einige mächtige Funktionen bereit.

**<R,A> R collect(Collector<? super T,A,R> collector)**

Im letzten Beispiel haben wir einfach die Ergebnisse wieder in eine Liste gepackt.
Dies ist wohl einer der einfachsten und häufigsten Anwendungsfälle.

Die Standard-Collectors aus dem Paket **java.util.stream.Collectors** sind:

 - toList()
 - toSet()
 - toCollection()
 - toMap()

Hierbei sind **toList()** und **toSet()** fest vorgegeben, möchten wir speziell collecten,
so sollten wir zu den Methoden toCollection() oder toMap() greifen

### Collectors.toMap()

Um einen Stream in eine Map zu überführen müssen wir der Methode toMap zwei Funktionen
übergeben, wobei die erste für die Erzeugung der keys und die zweite für die
Identifikation der Werte zuständig ist. Im Prinzip übergeben wir also einen keyMapper
und einen valueMapper.

```java

public Map<String, Integer> mapFarbeToLength(List<Car> cars) {
    return cars.stream().map(c -> c.getFarbe()).collect(Collectors.toMap(Function.identity(), String::length));
}

```
Im Beispiel ist **Function.identity()** der keyMapper und **String::length** 
der valueMapper. Ergebnis des Beispiels ist eine Map die zu jeder Farbe die Länge
des Strings enthält. Bei Konflikten wird allerdings eine **IllegalStateException**
geworfen. Um dies zu verhindern kann man noch einen dritten Parameter übergeben, 
einen Binary Operator.

```java

public Map<String, Integer> mapFarbeToLength(List<Car> cars) {
    return cars.stream().map(c -> c.getFarbe())
        .collect(Collectors.toMap(Function.identity(), String::length, (farbe, duplicate) -> farbe));
}

```

Der Nutzen der toMap-Methode hält sich jedoch in Grenzen, da es ein viel besseres
Feature gibt, um Maps komfortabler und spezialisierter zu erzeugen.

### Collectors.groupingBy()

Um eine Map mit komplexeren Anforderungen zu befüllen, können wire die groupingBy-Methode
verwenden.

```java

public Map<String, List<Car>> groupCarsByManufacturer(List<Car> cars) {
    return cars.stream()
        .collect(Collectors.groupingBy(c -> c.getManufacturer().getName());
}

```

### Collectors.joining()

Dieser Collector kann genutzt werden um Strings zu konkatinieren.

``` java

List<String> names = Arrays.asList("Mathew", "Peter", "Patrick");

//Joining

String result = names.stream().collect(Collectors.joining());
// "MathewPeterPatrick"

String result = names.stream().collect(Collectors.joining(", "));
// "Mathew, Peter, Patrick"

String result = names.stream().collect(Collectors.joining(", ", "Names are ", "!"));
// "Names are Mathew, Peter, Patrick!
```

### Collectors für Zahlen

Für Zahlen Streams, also Double,Int und Long, gibt es einige vorgefertigte Operationen.
Diese sind soweit selbsterklärend und werden daher hier nur aufgelistet:

- averganingDouble/Long/Int/()
- summingDouble/Long/Int/()
- maxBy/minBy(Comparator)
- summarizingDouble/Long/Int/()

Die Methode summarizingDouble/Long/Int/() ist etwas spezieller, weshalb ich diese
kurz erklären möchte. Der Rückgabewert dieser Methode ist bspw. DoubleSummaryStatistics.
Dieser enthält statistische Informationen über die Zahlen aus dem Stream:

```java

DoubleSummaryStatistics result = employee.stream()
    .collect(Collectors.summarizingDouble(Employee::getSalary))

assertThat(result.getSum()).isEqualTo(100000);
assertThat(result.getCount()).isEqualTo(10);
assertThat(result.getMin()).isEqualTo(10);
assertThat(result.getMax()).isEqualTo(40000);
assertThat(result.getAverage()).isEqualTo(20000);

```

## Sortieren von Streams

Ein häufiger Anwendungsfall ist auch das Sortieren von Elementen einer Liste. Häufig 
wird dies mit for-Schleifen gelöst. Dies ist allerdings nicht nötig. Auch diesen
Anwendungsfall können wir mit Streams abdecken.

```java

    List<String> list = Arrays.asList("3", "A", "c", "B", "1");

    List<String> sortedList1 = list.stream().sorted(Comparator.naturalOrder()).collect(Collectors.toList());
    List<String> sortedList2 = list.stream().sorted((o1, o2) -> o1.compareTo(o2)).collect(Collectors.toList());
    List<String> sortedList3 = list.stream().sorted().collect(Collectors.toList());

```

Alle Listen im obigen Beispiel sortieren nach der gleichen Bedingung, das bedeutet, geben wir der sorted()-Methode 
keinen Comparator mit, wird automatisch nach Comparator.naturalOrder() gefiltert. Die Angabe wäre also redundant.
Bei der zwieten Liste gilt das ebenfalls, allerding kann man an diesem Beispiel sehen, wie wir, sofern das
benötigt wird, einen eigenen Comparator nutzen können, wir mussen nur einen Lambda-Ausdruck verwenden.

Es gibt noch weitere Vorgefertigte Comparators:

- reverseOrder()
- nullsFirst(Comparator)
- nullsLast(Comparator)
- comparingInt/Long/Double(comparingFunction)

Angenommen wir haben wieder eine Liste von Autos, wovon einige keine Angabe über die PS-Zahl haben, so 
können wir folgendermaßen nach PS Zahl sortieren

```java

    List<String> cars = list.stream()
        .sorted(Comparator.nullsFirst(Comparator.comparingInt(Car::getHp)))
        .collect(Collectors.toList());
   
```



