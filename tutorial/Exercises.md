# Aufgaben

Für die Funktionalität die in den Übungen implementiert werden soll gibt es Tests,
die zu Beginn der Übung logischerweise rot sind. Diese gilt es nach und nach grün
zu bekommen. Im Normalfall gibt es pro Übung einen oder zwei Tests.
Um die Methoden testen zu können gibt es einen DataCreator. Dieser legt Testdaten
an, die für die Tests verwendet werden können.
Um die Aufgaben zu lösen darf nur der Code in der Klasse StreamTutorial verändert
werden. In dieser Klasse sind die Methoden bereits definiert, nur deren Implementierung
fehlt. Die Signaturen dürfen nicht angepasst werden. In den Model-Klassen ist
keine Änderung notwendig, genauso sollte auch an den Testdaten nichts verändert
werden. Wer die Tests verändert kommt ebenso in die
Hölle.

## Streams

1. Gebe alle Personen zurück die einen bestimmten Nachnamen haben.
2. Gebe alle Personen zurück die einen bestimmten Vor- und Nachnamen haben.
3. Implementiere Aufgabe 2 mit Predicates.
4. Erzeuge eine Liste aller Adressen aller Personen.
5. Erzeuge eine Liste unterschiedlicher Adressen aller Personen.
6. Liefere die erste Person die in einer bestimmten Stadt lebt.
7. Erzeuge eine Liste die alle Personen enthält die in der Liste von Personenlisten enthalten sind.
8. Zähle die Personen die in einer bestimmten Stadt leben.
9. Liefere alle Personen einer Stadt nach Nachnamen sortiert.
10. Prüfe ob eine Person in einer Stadt lebt.
11. Gruppiere alle Personen nach Städten.
12. Erzeuge eine sortierte, kommagetrennte Liste aller Vornamen.

## Optionals

13. Gebe den Namen des Wohnortes aus, wenn ein Wert fehlt gebe den Namen der Default-City (Diese ist als Konstante in der Klasse).
14. Implementiere 13 aber mit Aufruf der Methode des DataCreators, der die Default-City liefert.
15. Gebe den Straßennamen des Wohnortes einer Person zurück, wenn dieser Wert nicht geholt werden kann, werfe eine Exception mit der Nachricht "Keine Adresse gesetzt".