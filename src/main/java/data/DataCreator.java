package data;

import java.util.Arrays;
import java.util.List;

import models.Address;
import models.City;
import models.Person;

public class DataCreator {

    public static final City KARLSRUHE = new City("Karlsruhe", "76131");
    public static final City STUTENSEE = new City("Stutensee", "76297");
    public static final City KIRCHHEIM = new City("Kirchheim", "73230");
    public static final City STUTTGART = new City("Stuttgart", "70173");
    public static final City DEFAULT_CITY = new City("Keine Stadt bekannt", "00000");

    public static final Address ADRESSE_1 = new Address("Gustavstra�e", 4, KARLSRUHE);
    public static final Address ADRESSE_2 = new Address("Heinrichstra�e", 1, KARLSRUHE);
    public static final Address ADRESSE_3 = new Address("Hauptstra�e", 44, KARLSRUHE);
    public static final Address ADRESSE_4 = new Address("Veilchenweg", 14, KARLSRUHE);
    public static final Address ADRESSE_5 = new Address("Albert-Nestler-Stra�e", 22, KARLSRUHE);
    public static final Address ADRESSE_6 = new Address("Leibnitz Allee", 110, STUTENSEE);
    public static final Address ADRESSE_7 = new Address("Lorentz Stra�e", 1, STUTENSEE);
    public static final Address ADRESSE_8 = new Address("�tlinger Stra�e", 8, KIRCHHEIM);
    public static final Address ADRESSE_9 = new Address("Kirchheimer Stra�e", 20, KIRCHHEIM);
    public static final Address ADRESSE_10 = new Address("Willhelmstra�e", 123, KIRCHHEIM);

    public static final Person PERSON_1 = new Person("Pascal", "Kanzleiter", ADRESSE_1);
    public static final Person PERSON_2 = new Person("Gerda", "M�ller", ADRESSE_1);
    public static final Person PERSON_3 = new Person("Bastian", "Schweinsteiger", ADRESSE_2);
    public static final Person PERSON_4 = new Person("Bianca", "Kanzleiter", ADRESSE_2);
    public static final Person PERSON_5 = new Person("Karl", "M�ller", ADRESSE_2);
    public static final Person PERSON_6 = new Person("Lukas", "Podolski", ADRESSE_3);
    public static final Person PERSON_7 = new Person("Thomas", "M�ller", ADRESSE_4);
    public static final Person PERSON_8 = new Person("Manuel", "Neuer", ADRESSE_5);
    public static final Person PERSON_9 = new Person("Jerome", "Boateng", ADRESSE_6);
    public static final Person PERSON_10 = new Person("Marco", "Reus", ADRESSE_7);
    public static final Person PERSON_11 = new Person("Robert", "Lewandowski", ADRESSE_8);
    public static final Person PERSON_12 = new Person("Carl", "Cannelloni", ADRESSE_9);
    public static final Person PERSON_13 = new Person("Stefan", "Schmidt", ADRESSE_10);
    public static final Person PERSON_14 = new Person("Stefanie", "Schmidt", ADRESSE_10);

    public static List<Person> getAllePersonen() {
        return Arrays.asList(PERSON_1, PERSON_2, PERSON_3, PERSON_4, PERSON_5, PERSON_6, PERSON_7, PERSON_8, PERSON_9, PERSON_10,
                PERSON_11, PERSON_12, PERSON_13, PERSON_14);
    }

    public static List<Person> getKarlsruher() {
        return Arrays.asList(PERSON_1, PERSON_2, PERSON_3, PERSON_4, PERSON_5, PERSON_6, PERSON_7, PERSON_8);
    }

    public static List<Person> getKirchheimer() {
        return Arrays.asList(PERSON_11, PERSON_12, PERSON_13, PERSON_14);
    }

    public City getDefaultCity() {
        return DEFAULT_CITY;
    }
}
