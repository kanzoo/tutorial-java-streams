package optionals;

import data.DataCreator;
import models.City;
import models.Person;

public class OptionalTutorial {

    private static final City DEFAULT_CITY = DataCreator.DEFAULT_CITY;

    private DataCreator dataCreator = new DataCreator();

    public String getHomeTownNameOfPerson(Person person) {
        return null;
    }

    public String getHomeTownNameOfPersonWithDefaultMethod(Person person) {
        return null;
    }

    public String getStreetNameOfPerson(Person person) throws Exception {
        return null;
    }

}
