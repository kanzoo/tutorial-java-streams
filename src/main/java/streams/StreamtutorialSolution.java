package streams;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import data.DataCreator;
import models.Address;
import models.City;
import models.Person;

public class StreamtutorialSolution {

    private List<Person> allPersons = DataCreator.getAllePersonen();

    public List<Person> getPersonsByLastName(String name) {
        return allPersons.stream().filter(person -> person.getLastName().equals(name)).collect(Collectors.toList());
    }

    public List<Person> getPersonsByFullName(String firstName, String lastName) {
        return allPersons.stream()
                .filter(person -> person.getFirstName().equals(firstName))
                .filter(person -> person.getLastName().equals(lastName))
                .collect(Collectors.toList());
    }

    public List<Person> getPersonsByFullNameWithPredicates(String firstName, String lastName) {
        Predicate<Person> firstNamePredicate = (Person p) -> p.getFirstName().equals(firstName);
        Predicate<Person> lastNamePredicate = (Person p) -> p.getLastName().equals(lastName);

        return allPersons.stream().filter(firstNamePredicate.and(lastNamePredicate)).collect(Collectors.toList());
    }

    public List<Address> getAddressesOfPersons() {
        return allPersons.stream().map(Person::getAdress).collect(Collectors.toList());
    }

    public List<Address> getDistinctAddressesOfPersons() {
        return allPersons.stream().map(Person::getAdress).distinct().collect(Collectors.toList());
    }

    public Person getFirstPersonFromCity(City city) throws Exception {
        return allPersons.stream()
                .filter(p -> p.getAdress().getCity().equals(city))
                .findFirst()
                .orElseThrow(() -> new Exception("Not found"));
    }

    public List<Person> createPersonList(List<List<Person>> personen) {
        return personen.stream().flatMap(Collection::stream).collect(Collectors.toList());
    }

    public long countPersonsOfCity(City city) {
        return allPersons.stream().filter(p -> p.getAdress().getCity().equals(city)).count();
    }

    public List<Person> sortPersonsOfCityByNachname(City city) {
        return allPersons.stream()
                .filter(p -> p.getAdress().getCity().equals(city))
                .sorted((p1, p2) -> p1.getLastName().compareTo(p2.getLastName()))
                .collect(Collectors.toList());
    }

    public boolean checkPersonLivesInCity(String firstName, String lastName, City city) {
        return allPersons.stream()
                .filter(p -> p.getAdress().getCity().equals(city))
                .anyMatch(p -> p.getFirstName().equals(firstName) && p.getLastName().equals(lastName));
    }

    public Map<String, List<Person>> groupPersonsByCity() {
        return allPersons.stream().collect(Collectors.groupingBy(p -> p.getAdress().getCity().getName()));
    }

    public String createSortedFirstNameList() {
        return allPersons.stream().map(Person::getFirstName).sorted().collect(Collectors.joining(", ")).toString();
    }

}
