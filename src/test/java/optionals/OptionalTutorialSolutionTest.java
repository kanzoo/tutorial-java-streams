package optionals;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import data.DataCreator;
import models.Address;
import models.City;
import models.Person;

public class OptionalTutorialSolutionTest {

    private OptionalTutorialSolution underTest;
    private Person personWithoutAdress;
    private Person personWithoutAdressCity;
    private Person personWithoutAdressCityName;
    private Person personAusKarlsruhe = DataCreator.PERSON_1;

    @BeforeEach
    void setUp() {
        underTest = new OptionalTutorialSolution();

        personWithoutAdress = new Person("test", "test", null);
        personWithoutAdressCity = new Person("test", "test", new Address("test", 1, null));
        personWithoutAdressCityName = new Person("test", "test", new Address("test", 1, new City(null, "00000")));
    }

    @Test
    void testGetHomeTownNameOfPerson() {
        String result = underTest.getHomeTownNameOfPerson(personAusKarlsruhe);

        assertThat(result).isEqualTo(DataCreator.KARLSRUHE.getName());
    }

    @Test
    void testGetHomeTownNameOfPersonThrowsNoNPE() {
        String result1 = underTest.getHomeTownNameOfPerson(personWithoutAdress);
        String result2 = underTest.getHomeTownNameOfPerson(personWithoutAdressCity);
        String result3 = underTest.getHomeTownNameOfPerson(personWithoutAdressCityName);

        assertThat(result1).isEqualTo(DataCreator.DEFAULT_CITY.getName());
        assertThat(result2).isEqualTo(DataCreator.DEFAULT_CITY.getName());
        assertThat(result3).isEqualTo(DataCreator.DEFAULT_CITY.getName());
    }

    @Test
    void testGetHomeTownNameOfPersonWithDefaultMethod() {
        String result = underTest.getHomeTownNameOfPersonWithDefaultMethod(personAusKarlsruhe);

        assertThat(result).isEqualTo(DataCreator.KARLSRUHE.getName());
    }

    @Test
    void testGetHomeTownNameOfPersonWithDefaultMethodThrowsNoNPE() {
        String result1 = underTest.getHomeTownNameOfPersonWithDefaultMethod(personWithoutAdress);
        String result2 = underTest.getHomeTownNameOfPersonWithDefaultMethod(personWithoutAdressCity);
        String result3 = underTest.getHomeTownNameOfPersonWithDefaultMethod(personWithoutAdressCityName);

        assertThat(result1).isEqualTo(DataCreator.DEFAULT_CITY.getName());
        assertThat(result2).isEqualTo(DataCreator.DEFAULT_CITY.getName());
        assertThat(result3).isEqualTo(DataCreator.DEFAULT_CITY.getName());
    }

    @Test
    void testGetStreetNameOfPerson() throws Exception {
        String result = underTest.getStreetNameOfPerson(personAusKarlsruhe);

        assertThat(result).isEqualTo(DataCreator.ADRESSE_1.getStreetName());
    }

    @Test
    void testGetStreetNameOfPersonThrowsExceptionIfNotFound() throws Exception {
        assertThrows(Exception.class, () -> {
            underTest.getStreetNameOfPerson(personWithoutAdress);
        });
    }

}
