# Java Streams & Optionals Tutorial

In diesem Tutorial sollen die Möglichkeiten, die die Streams aus Java 8 bieten, aufgezeigt
werden und am Ende in kleinen praktischen Übungen angewendet werden. Außerdem gibt es noch
ein kleines Tutorial zu den Optionals die ebenfalls aus Java 8 stammen, und gerade im 
Zusammenhang mit den Streams sehr interessante Funktionalitäten bieten.

## Vorraussetzungen

- Java-Kenntnisse
- JDK 1.8 oder höher
- Maven
- Eclipse

## Installation

1. Auschecken des Codes
2. Projekt in Eclipse importieren  
    Eclipse > Import > Existing Maven Project 
3. Dependencies installieren
    Run as > Maven install

## Tutorial

[Stream Tutorial](tutorial/Streams.md)  
[Optionals Tutorial](tutorial/Optionals.md)  
[Hier gehts zu den Aufgaben](tutorial/Exercises.md)
