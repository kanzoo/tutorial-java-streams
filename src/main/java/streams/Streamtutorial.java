package streams;

import java.util.List;
import java.util.Map;

import data.DataCreator;
import models.Address;
import models.City;
import models.Person;

public class Streamtutorial {

    private List<Person> allPersons = DataCreator.getAllePersonen();

    public List<Person> getPersonsByLastName(String name) {
        return null;
    }

    public List<Person> getPersonsByFullName(String firstName, String lastName) {
        return null;
    }

    public List<Person> getPersonsByFullNameWithPredicates(String firstName, String lastName) {
        return null;
    }

    public List<Address> getAddressesOfPersons() {
        return null;
    }

    public List<Address> getDistinctAddressesOfPersons() {
        return null;
    }

    public Person getFirstPersonFromCity(City city) throws Exception {
        return null;
    }

    public List<Person> createPersonList(List<List<Person>> personen) {
        return null;
    }

    public long countPersonsOfCity(City city) {
        return 0;
    }

    public List<Person> sortPersonsOfCityByNachname(City city) {
        return null;
    }

    public boolean checkPersonLivesInCity(String firstName, String lastName, City city) {
        return false;
    }

    public Map<String, List<Person>> groupPersonsByCity() {
        return null;
    }

    public String createSortedFirstNameList() {
        return null;
    }

}
